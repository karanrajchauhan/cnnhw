import tensorflow as tf


def logistic_regression_map(x_):
    scope_args = {'initializer': tf.random_normal_initializer(stddev=1e-4)}
    with tf.variable_scope("weights", **scope_args):
        flattered = tf.contrib.layers.flatten(x_, scope='pool2flat')
        W = tf.get_variable('W', shape=[3072, 10])
        b = tf.get_variable('b', shape=[10])
        y_logits = tf.matmul(flattered, W) + b
    return y_logits


def cnn_map(x_, conv1_filters=32, conv1_kernel_size=(5, 5), conv1_strides=(1, 1), pool1_strides=2, conv2_filters=32,
            conv2_kernel_size=(5, 5), conv2_strides=(1, 1), pool2_strides=2, fc_units=500):
    conv1 = tf.layers.conv2d(
        inputs=x_,
        filters=conv1_filters,
        kernel_size=conv1_kernel_size,
        strides=conv1_strides,
        padding="same",
        activation=tf.nn.relu)
    pool1 = tf.layers.max_pooling2d(inputs=conv1,
                                    pool_size=[2, 2],
                                    strides=pool1_strides)
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=conv2_filters,
        kernel_size=conv2_kernel_size,
        strides=conv2_strides,
        padding="same",
        activation=tf.nn.relu)
    pool2 = tf.layers.max_pooling2d(inputs=conv2,
                                    pool_size=[2, 2],
                                    strides=pool2_strides)
    pool_flat = tf.contrib.layers.flatten(pool2, scope='pool2flat')
    dense = tf.layers.dense(inputs=pool_flat, units=fc_units, activation=tf.nn.relu)
    logits = tf.layers.dense(inputs=dense, units=10)
    return logits


def cnn_expanded(x_):
    conv1 = tf.layers.conv2d(inputs=x_,
                             filters=32,
                             kernel_size=(5,5),
                             strides=(1,1),
                             padding='same',
                             activation=tf.nn.relu)
    maxpool1 = tf.layers.max_pooling2d(inputs=conv1,
                                       pool_size=(2,2),
                                       strides=(2,2))
    conv2 = tf.layers.conv2d(inputs=maxpool1,
                             filters=32,
                             kernel_size=(5,5),
                             strides=(1,1),
                             padding='same',
                             activation=tf.nn.relu)
    maxpool2 = tf.layers.max_pooling2d(inputs=conv2,
                                       pool_size=(2,2),
                                       strides=(2,2))
    conv3 = tf.layers.conv2d(inputs=maxpool2,
                             filters=64,
                             kernel_size=(3,3),
                             strides=(1,1),
                             padding='same',
                             activation=tf.nn.relu)
    maxpool3 = tf.layers.max_pooling2d(inputs=conv3,
                                       pool_size=(2,2),
                                       strides=(2,2))
    pool3_flattened = tf.contrib.layers.flatten(maxpool3, scope='pool2flat')
    fc = tf.layers.dense(inputs=pool3_flattened, units=300, activation=tf.nn.relu)
    logits = tf.layers.dense(inputs=fc, units=10)
    return logits


def apply_classification_loss(model_function, add_summary=False):
    with tf.Graph().as_default() as g:
        with tf.device("/gpu:0"):  # use gpu:0 if on GPU
            x_ = tf.placeholder(tf.float32, [None, 32, 32, 3])
            y_ = tf.placeholder(tf.int32, [None])
            y_logits = model_function(x_)
            
            y_dict = dict(labels=y_, logits=y_logits)
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(**y_dict)
            cross_entropy_loss = tf.reduce_mean(losses)
            trainer = tf.train.AdamOptimizer()
            train_op = trainer.minimize(cross_entropy_loss)
            
            y_pred = tf.argmax(tf.nn.softmax(y_logits), axis=1)
            correct_prediction = tf.equal(tf.cast(y_pred, tf.int32), y_)
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

            #merged = None
            #if add_summary:
        tf.summary.scalar('loss', cross_entropy_loss)
        tf.summary.scalar('accuracy', accuracy)
        merged = tf.summary.merge_all()
    
    model_dict = {'graph': g, 'inputs': [x_, y_], 'train_op': train_op,
                  'accuracy': accuracy, 'loss': cross_entropy_loss, 'merged': merged}

    return model_dict
