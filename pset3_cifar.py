import tensorflow as tf
import numpy as np
import read_data
import model


def train_model(model_dict, dataset_generators, epoch_n, print_every):
    with model_dict['graph'].as_default(), tf.Session() as sess:
                
        # summary writer
        merged = model_dict['merged']
        train_summary_writer = tf.summary.FileWriter('summaries/train', g)
        # test_summary_writer = tf.summary.FileWriter('summaries/test', g)

        sess.run(tf.global_variables_initializer())
        
        for epoch_i in range(epoch_n):
            for iter_i, data_batch in enumerate(dataset_generators['train']):
                train_feed_dict = dict(zip(model_dict['inputs'], data_batch))
                sess.run(model_dict['train_op'], feed_dict=train_feed_dict)
                
                if iter_i % print_every == 0:
                    collect_arr = []
                    for test_batch in dataset_generators['test']:
                        test_feed_dict = dict(zip(model_dict['inputs'], test_batch))
                        to_compute = [model_dict['loss'], model_dict['accuracy']]
                        summary, loss_accuracy = sess.run(to_compute, test_feed_dict)
                        collect_arr.append(loss_accuracy)

                    train_summary_writer.add_summary(summary)
                    averages = np.mean(collect_arr, axis=0)
                    fmt = (epoch_i, iter_i, ) + tuple(averages)
                    print('epoch {:d} iter {:d},  loss: {:.3f}, '
                          'accuracy: {:.3f}'.format(*fmt))


def new_train_model(model_dict, dataset_generators, epoch_n, print_every,
                    save_model=False, load_model=False, save_path=None):
    if save_path is None:
        save_path = 'checkpoints/cnn_expanded.ckpt'

    with model_dict['graph'].as_default(), tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        # saver object to save model
        saver = tf.train.Saver()
        
        if load_model:
            saver.restore(sess, save_path)
            
        for epoch_i in range(epoch_n):
            for iter_i, data_batch in enumerate(dataset_generators['train']):
                train_feed_dict = dict(zip(model_dict['inputs'], data_batch))
                sess.run(model_dict['train_op'], feed_dict=train_feed_dict)
                
                if iter_i % print_every == 0:
                    collect_arr = []
                    for test_batch in dataset_generators['test']:
                        test_feed_dict = dict(zip(model_dict['inputs'], test_batch))
                        to_compute = [model_dict['loss'], model_dict['accuracy']]
                        collect_arr.append(sess.run(to_compute, test_feed_dict))
                    averages = np.mean(collect_arr, axis=0)
                    fmt = (epoch_i, iter_i, ) + tuple(averages)
                    print('iteration {:d} {:d}\t loss: {:.3f}, '
                          'accuracy: {:.3f}'.format(*fmt))
                    
        if save_model:
            saver.save(sess, save_path)


def main():

    # ================================= DATASET GENERATORS =================================== #

    # FOR CIFAR-10
    cifar10_dataset_generators = {
        'train': read_data.cifar10_dataset_generator('train', 1000),
        'test': read_data.cifar10_dataset_generator('test', -1)
    }
    
    # # FOR SVHN
    # dataset_generators = {
    #     'train': read_data.svhn_dataset_generator('train', 512),
    #     'test': read_data.svhn_dataset_generator('test', 512)
    # }

    # deeper cnn for cifar
    cnn_expanded_dict = model.apply_classification_loss(model.cnn_expanded, add_summary=False)
   

    # ==================================== TF MODEL SAVER ==================================== #

    # ### Hint: call the saver like this: tf.train.Saver(var_list)
    # ### where var_list is a list of TF variables you want to save
    # new_train_model(cnn_expanded_dict, dataset_generators, epoch_n=100, print_every=10, save_model=True)

    # ### Hint: call the saver like this: tf.train.Saver(var_list)
    # ### where var_list is a list of TF variables you want to load from the checkpoint 
    # new_train_model(cnn_expanded_dict, dataset_generators, epoch_n=10, print_every=1, load_model=True)


    # ====================================== CIFAR CNN ======================================= #

    # train the model from scratch
    new_train_model(cnn_expanded_dict, cifar10_dataset_generators, epoch_n=500, print_every=10)
        
    # fine-tuning SVHN Net using Cifar-10 weights saved in Q2
    new_train_model(cnn_expanded_dict, cifar10_dataset_generators, epoch_n=500, print_every=10, load_model=True)
 

    # ======================================= SVHN CNN ======================================= #
 
    # # vanilla
    # model_dict = model.apply_classification_loss(model.cnn_map)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=1)

    # # filters - 64, 64
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=64, conv2_filters=64)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 128, 32
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=128, conv2_filters=32)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 32, 128
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=32, conv2_filters=128)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # kernel - 3x3, 3x3
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(3,3), conv2_kernel_size=(3,3))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # kernel - 5x5, 11x11
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(5,5), conv2_kernel_size=(11,11))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 3x3,5x5
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(3,3), conv2_kernel_size=(5,5))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)


main()
