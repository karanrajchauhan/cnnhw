import tensorflow as tf
import numpy as np
import read_data
import model
import pdb
import matplotlib.pyplot as plt

def train_model(model_dict, dataset_generators, epoch_n, print_every):
    with model_dict['graph'].as_default(), tf.Session() as sess:
        # summary writer
        merged = model_dict['merged']
        train_summary_writer = tf.summary.FileWriter('summaries/train', sess.graph)
        print(type(merged))
        sess.run(tf.global_variables_initializer())
        
        for epoch_i in range(epoch_n):
            for iter_i, data_batch in enumerate(dataset_generators['train']):
                train_feed_dict = dict(zip(model_dict['inputs'], data_batch))
                summary, _ = sess.run([merged, model_dict['train_op']], feed_dict=train_feed_dict)
                train_summary_writer.add_summary(summary)
                
                if iter_i % print_every == 0:
                    collect_arr = []
                    for test_batch in dataset_generators['test']:
                        test_feed_dict = dict(zip(model_dict['inputs'], test_batch))
                        to_compute = [model_dict['loss'], model_dict['accuracy']]
                        collect_arr.append(sess.run(to_compute, test_feed_dict))

                    averages = np.mean(collect_arr, axis=0)
                    fmt = (epoch_i, iter_i, ) + tuple(averages)
                    print('epoch {:d} iter {:d},  loss: {:.3f}, '
                          'accuracy: {:.3f}'.format(*fmt))


def new_train_model(model_dict, dataset_generators, epoch_n, print_every,
                    save_model=False, load_model=False, save_path=None):
    if save_path is None:
        save_path = 'checkpoints/cnn_expanded.ckpt'

    with model_dict['graph'].as_default(), tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        # saver object to save model
        saver = tf.train.Saver()
        
        if load_model:
            saver.restore(sess, save_path)
            
        for epoch_i in range(epoch_n):
            for iter_i, data_batch in enumerate(dataset_generators['train']):
                train_feed_dict = dict(zip(model_dict['inputs'], data_batch))
                sess.run(model_dict['train_op'], feed_dict=train_feed_dict)
                
                if iter_i % print_every == 0:
                    collect_arr = []
                    for test_batch in dataset_generators['test']:
                        test_feed_dict = dict(zip(model_dict['inputs'], test_batch))
                        to_compute = [model_dict['loss'], model_dict['accuracy']]
                        collect_arr.append(sess.run(to_compute, test_feed_dict))
                    averages = np.mean(collect_arr, axis=0)
                    fmt = (epoch_i, iter_i, ) + tuple(averages)
                    print('iteration {:d} {:d}\t loss: {:.3f}, '
                          'accuracy: {:.3f}'.format(*fmt))
                    
        if save_model:
            saver.save(sess, save_path)


def visualize_images(model_dict, dataset_generators, save_path=None, load_model=False):

    # use default path if no path given and load required
    if load_model and save_path is None:
        save_path = 'checkpoints/cnn_expanded.ckpt'

    with model_dict['graph'].as_default(), tf.Session() as sess:
        # image summary writer
        image_summary_writer = tf.summary.FileWriter('image_summaries/train', sess.graph)

        # init
        sess.run(tf.global_variables_initializer())

        # load model if required
        if load_model:
            saver = tf.train.Saver()
            saver.restore(sess, save_path)

        # input image to feed to graph
        for data_batch in dataset_generators['train']:
            # take the first image from batch
            x = data_batch[0][np.newaxis, 0, :, :, :]
            y = data_batch[1][np.newaxis, 0]

            # get output of first conv2d layer
            feed_dict = dict(zip(model_dict['inputs'], (x, y)))
            conv1 = sess.graph.get_tensor_by_name('conv2d/Relu:0')
            conv1_output = sess.run(conv1, feed_dict)

            # reshape and feed into summary
            conv1_output_shape = conv1_output.shape
            conv1_reshaped = tf.reshape(conv1_output, [-1, conv1_output_shape[1], conv1_output_shape[2], 1])
            image_summary = tf.summary.image('conv1_summ', conv1_reshaped)
            image_summary_writer.add_summary(image_summary.eval())

            # save activation images
            vmin=conv1_output.min()
            vmax=conv1_output.max()
            for filter_num in range(conv1_output_shape[3]):
                plt.imsave('activation_images/filter{}.png'.format(filter_num), conv1_output[0,:,:,filter_num], vmin=vmin, vmax=vmax, cmap='gray')

            # save kernel images
            kernels = sess.graph.get_tensor_by_name('conv2d/kernel:0').eval()
            # biases = sess.graph.get_tensor_by_name('conv2d/bias:0').eval()
            # kernel_vmin = kernels.min()
            # kernel_vmax = kernels.max()
            for kernel_num in range(kernels.shape[3]):
                for chan in range(3):
                    plt.imsave('kernel_images/kernel{}_c{}.png'.format(kernel_num, chan), kernels[:,:,chan,kernel_num],  cmap='gray')
            break


def main():

    # ================================= DATASET GENERATORS =================================== #

    # # FOR CIFAR-10
    # dataset_generators = {
    #     'train': read_data.cifar10_dataset_generator('train', 512),
    #     'test': read_data.cifar10_dataset_generator('test', -1)
    # }
    
    # FOR SVHN
    dataset_generators = {
        'train': read_data.svhn_dataset_generator('train', 512),
        'test': read_data.svhn_dataset_generator('test', 512)
    }

    # deeper cnn
    cnn_expanded_dict = model.apply_classification_loss(model.cnn_expanded)
   

    # ==================================== TF MODEL SAVER ==================================== #

    # ### Hint: call the saver like this: tf.train.Saver(var_list)
    # ### where var_list is a list of TF variables you want to save
    # new_train_model(cnn_expanded_dict, dataset_generators, epoch_n=100, print_every=10, save_model=True)

    # ### Hint: call the saver like this: tf.train.Saver(var_list)
    # ### where var_list is a list of TF variables you want to load from the checkpoint 
    # new_train_model(cnn_expanded_dict, dataset_generators, epoch_n=1, print_every=1, load_model=True)


    # ====================================== CIFAR CNN ======================================= #

    # # train the model from scratch
    # new_train_model(cnn_expanded_dict, cifar10_dataset_generators, epoch_n=100, print_every=10)
        
    # # fine-tuning SVHN Net using Cifar-10 weights saved in Q2
    # new_train_model(cnn_expanded_dict, cifar10_dataset_generators, epoch_n=100, print_every=10, load_model=True)
 

    # ======================================= SVHN CNN ======================================= #
 
    # # vanilla
    # model_dict = model.apply_classification_loss(model.cnn_map, add_summary=True)
    # train_model(model_dict, dataset_generators, epoch_n=25, print_every=1)

    # visualize images
    visualize_images(cnn_expanded_dict, dataset_generators, load_model=True)

    # # filters - 64, 64
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=64, conv2_filters=64)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 128, 32
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=128, conv2_filters=32)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 32, 128
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_filters=32, conv2_filters=128)
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # kernel - 3x3, 3x3
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(3,3), conv2_kernel_size=(3,3))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # kernel - 5x5, 11x11
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(5,5), conv2_kernel_size=(11,11))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)

    # # filters - 3x3,5x5
    # model_dict = model.apply_classification_loss(model.cnn_map, conv1_kernel_size=(3,3), conv2_kernel_size=(5,5))
    # train_model(model_dict, dataset_generators, epoch_n=1, print_every=10)


main()
